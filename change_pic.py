import configparser
from os import path, listdir
import os
import shutil
from sys import argv
from datetime import datetime, timezone, timedelta
import random
from pyrogram import Client
import cv2


config = configparser.ConfigParser()
config.read("config.ini")


def compare_img_cv2(im1, im2):
    if im1.shape == im2.shape:
        difference = cv2.subtract(im1, im2)
        b, g, r = cv2.split(difference)
        if cv2.countNonZero(b) == 0 and cv2.countNonZero(g) == 0 and cv2.countNonZero(r) == 0:
            return True
        else:
            return False
    else:
        return False


def main(new_pick = False):
    if new_pick or not "cur_pic_to_set.png" in listdir("./"):
        if path.exists('pics_to_set') and listdir('pics_to_set') or path.exists('used_pics') and listdir('used_pics'):
            if path.exists('pics_to_set') and listdir('pics_to_set'):
                pic_to_set_name = f"pics_to_set/{listdir('pics_to_set')[random.randrange(len(listdir('pics_to_set')))]}"
            else:
                pic_to_set_name = f"used_pics/{listdir('used_pics')[random.randrange(len(listdir('pics_to_set')))]}"
            shutil.copy(pic_to_set_name, 'cur_pic_to_set.png')
            cv2_image = cv2.imread(pic_to_set_name)
            cv2_output = cv2_image.copy()
            cv2_overlay = cv2_image.copy()
            cv2_overlay = cv2.cvtColor(cv2_overlay, cv2.COLOR_BGR2GRAY)
            cv2_overlay = cv2.cvtColor(cv2_overlay, cv2.COLOR_GRAY2BGR)
            hours_now = datetime.now(timezone.utc).hour
            alpha = abs(hours_now - 12)/4
            cv2.addWeighted(cv2_overlay, alpha, cv2_output, 1 - alpha, 0, cv2_output)
            cv2.imwrite('tmp_pic_to_set.png', cv2_output)
            with Client(session_name="my_account", api_id=config['credentials']['pyrogram_api_id'],
                        api_hash=config['credentials']['pyrogram_api_hash']) as app:
                app.set_profile_photo('tmp_pic_to_set.png')
            pic_name = pic_to_set_name.split('/')[1]
            if pic_to_set_name.split('/')[0] != "used_pics":
                if pic_name in listdir('used_pics'):
                    pic_name = 0
                    while f'pic_n{pic_name}.png' in listdir('used_pics'):
                        pic_name += 1
                    pic_name = f'pic_n{pic_name}.png'
                os.rename(pic_to_set_name, f'used_pics/{pic_name}')
            print(f"{datetime.now().strftime('%d/%m/%y %H:%M')} Successfully changed pic, current pic name {pic_name}, alpha: {alpha}")
        else:
            print(f"{datetime.now().strftime('%d/%m/%y %H:%M')} Error, no pictures to set!")
    else:
        pic_to_set_name = "cur_pic_to_set.png"
        cv2_image = cv2.imread(pic_to_set_name)
        cv2_output = cv2_image.copy()
        cv2_overlay = cv2_image.copy()
        cv2_overlay = cv2.cvtColor(cv2_overlay, cv2.COLOR_BGR2GRAY)
        cv2_overlay = cv2.cvtColor(cv2_overlay, cv2.COLOR_GRAY2BGR)
        hours_now = datetime.now(timezone.utc).hour
        alpha = abs(hours_now - 12)/4
        cv2.addWeighted(cv2_overlay, alpha, cv2_output, 1 - alpha, 0, cv2_output)
        if "tmp_pic_to_set.png" in listdir('./'):
            img_to_check = cv2.imread("tmp_pic_to_set.png")
            if compare_img_cv2(img_to_check, cv2_output):
                print(f"{datetime.now().strftime('%d/%m/%y %H:%M')} Same image, do nothing, alpha: {alpha}")
                return
        cv2.imwrite('tmp_pic_to_set.png', cv2_output)
        with Client(session_name="my_account", api_id=config['credentials']['pyrogram_api_id'],
                    api_hash=config['credentials']['pyrogram_api_hash']) as app:
            prof_pic = app.get_profile_photos('me')[0]
            app.delete_profile_photos(prof_pic.file_id)
            app.set_profile_photo('tmp_pic_to_set.png')
        print(f"{datetime.now().strftime('%d/%m/%y %H:%M')} Successfully changed pic's alpha: {alpha}")


if __name__=="__main__":
    if len(argv)==2 and argv[1]:
        main(True)
    else:
        hours_now = datetime.now(timezone(timedelta(hours=3))).hour
        if hours_now == 8:
            main(True)
        else:
            main(False)
