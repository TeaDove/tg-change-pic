import mimetypes
from os import path, listdir
import re
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, InputMediaPhoto


def get_n():
    listNames = sorted(re.findall(r'pic_n(\d+).png', ' '.join(listdir('pics_to_set'))), key=int)
    return int(listNames[-1])+1


def is_url_image(url):
    mimetype, encoding = mimetypes.guess_type(url)
    return mimetype and mimetype.startswith('image')


def gen_reply_markup(dir_n, page_n):
    page_n = int(page_n)
    pic_names = [file.split('/')[1] for file in sorted([dir_n+'/'+file for file in listdir(dir_n)], key=path.getmtime)]  # Getting files in dir, sorted by date of last modification
    pic_names.reverse()
    buttonMatrix = []
    for index in range(page_n*9, min(((page_n+1)*9), len(pic_names))):
        buttonMatrix.append([InlineKeyboardButton(pic_names[index], callback_data=f'pic {dir_n}/{pic_names[index]} {page_n}')])
    buttonMatrix.append([InlineKeyboardButton('Current prof pic', callback_data=f'pic tmp_pic_to_set.png {page_n} {dir_n}'),
                             InlineKeyboardButton("Current pic's tmpl", callback_data=f'pic cur_pic_to_set.png {page_n} {dir_n}')])
    pageList = []
    if page_n > 0:
        pageList.append(InlineKeyboardButton("<<", callback_data= f'change_page {page_n-1} {dir_n}'))
    pageList.append(InlineKeyboardButton('pics_to_set' if dir_n=='used_pics' else 'used_pics', callback_data= f"cd {'pics_to_set' if dir_n=='used_pics' else 'used_pics'}"))
    if (page_n+1)*9 < len(pic_names):
        pageList.append(InlineKeyboardButton(">>", callback_data= f'change_page {page_n+1} {dir_n}'))
    buttonMatrix.append(pageList)
    return InlineKeyboardMarkup(buttonMatrix)
