import logging
import configparser
import mimetypes
import requests
from datetime import datetime
from os import listdir
import os
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, InputMediaPhoto
from telegram.ext import Updater, CommandHandler, MessageHandler, CallbackQueryHandler, Filters
import zipfile
import itertools
import utils

config = configparser.ConfigParser()
config.read("config.ini")

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)
    strToSend = f'Update from {update} caused error {context.error}'
    for index in range(0, len(strToSend) // 4000 + 1):
        context.bot.send_message(config.get('credentials', 'owner'), f'{strToSend[4000 * index:4000 * (index + 1)]}')


def ls(update, context):
    if update.effective_user.id == int(config.get('credentials', 'owner')):
        strToSend = "<b>cur_pic_to_set.png:</b>"
        update.message.reply_photo(open(f'cur_pic_to_set.png', 'rb'), caption=strToSend, parse_mode='html', reply_markup=utils.gen_reply_markup('pics_to_set', 0))


def download_all_pics(update, context):
    if update.effective_user.id == int(config.get('credentials', 'owner')):
        zipf = zipfile.ZipFile('tmp_img.zip', 'w', zipfile.ZIP_DEFLATED)
        for root, dirs, files in itertools.chain(os.walk('pics_to_set'), os.walk('used_pics'), os.walk('save_space')):
            for file in files:
                zipf.write(os.path.join(root, file))
        zipf.close()
        update.message.reply_document(open('tmp_img.zip', 'rb'))


def status(update, context):
    if update.effective_user.id == int(config.get('credentials', 'owner')):
        strToSend = f'Pics stat:\n' \
                    f'<b>{len(listdir("pics_to_set"))}</b> pics to set\n' \
                    f'<b>{len(listdir("used_pics"))}</b> pics already set\n' \
                    f'<b>{len(listdir("pics_to_set"))+len(listdir("used_pics"))}</b> in total'
        update.message.reply_text(strToSend, parse_mode='html')

def message_main(update, context):
    if update.effective_user.id == int(config.get('credentials', 'owner')):
        forFlag = True
        for entity in update.message.entities:
            if entity.type == "url":
                url = update.message.parse_entity(entity)
                if utils.is_url_image(url):
                    forFlag = False
                    file_to_save = requests.get(url)
                    pic_name = f'pic_n{context.bot_data["name"]}.png'
                    context.bot_data['name'] += 1
                    open(f'pics_to_set/{pic_name}', 'wb').write(file_to_save.content)
                    update.message.reply_text(f'Done, {pic_name} saved in pics_to_send/')
        if forFlag:
            update.message.reply_text(f'Send me URL of pic, if you wanna add pic')


def photo_handler(update, context):
    if update.effective_user.id == int(config.get('credentials', 'owner')):
        pic_name = f'pic_n{context.bot_data["name"]}.png'
        context.bot_data['name'] += 1
        update.message.photo[len(update.message.photo)-1].get_file().download(f"pics_to_set/{pic_name}")
        update.message.reply_text(f"Done, {pic_name} saved in pics_to_send/")


def callback_handler(update, context):
    if update.effective_user.id == int(config.get('credentials', 'owner')):
        callbackAnswer = ''
        callbackData = update.callback_query.data.split()
        queryTitle = callbackData[0]
        if queryTitle == 'change_page':
            update.callback_query.message.edit_reply_markup(reply_markup=utils.gen_reply_markup(callbackData[2], callbackData[1]))
        elif queryTitle == 'cd':
            update.callback_query.message.edit_reply_markup(reply_markup=utils.gen_reply_markup(callbackData[1], 0))
        elif queryTitle == 'pic':
            dir_name = '' if callbackData[1].find('/')==-1 else callbackData[1].split("/")[0]
            pic_name = callbackData[1].split('/')[1] if dir_name else callbackData[1]
            if pic_name in os.listdir(dir_name if dir_name else None):
                update.callback_query.message.edit_media(InputMediaPhoto(media=open(callbackData[1], 'rb')))
                if update.callback_query.message.caption != f'{callbackData[1]}:':
                    update.callback_query.message.edit_caption(caption=f'{callbackData[1]}:')
                if callbackData[1].find('/') != -1:
                    update.callback_query.message.edit_reply_markup(reply_markup=utils.gen_reply_markup(callbackData[1].split('/')[0], callbackData[2]))
                else:
                    update.callback_query.message.edit_reply_markup(reply_markup=utils.gen_reply_markup(callbackData[3], callbackData[2]))
            else:
                callbackAnswer = 'no such file in the dir'
        update.callback_query.answer(callbackAnswer)


def main():
    updater = Updater(config.get('credentials', 'telegram_token'), use_context=True)
    updater.bot.send_message(config.get('credentials', 'owner'), '<b>Hello World!</b>', parse_mode='HTML')
    dp = updater.dispatcher
    dp.bot_data['name'] = utils.get_n()
    dp.add_handler(CommandHandler('ls', ls))
    dp.add_handler(CommandHandler('download_all_pics', download_all_pics))
    dp.add_handler(CommandHandler('status', status))
    dp.add_handler(MessageHandler(Filters.text, message_main))
    dp.add_handler(MessageHandler(Filters.photo, photo_handler))
    dp.add_handler(CallbackQueryHandler(callback_handler))
    dp.add_error_handler(error)
    updater.start_polling()
    updater.idle()
    print('Hello World')


if __name__ == '__main__':
    main()
